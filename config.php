<?php
define('URL','http://localhost/iniciomvc/');
define('LIBS','libs/');
define('MODELS','./models');
define('MODULE','./views/modules/');
define('BS','./bussinesLogic');

define('DB_TYPE','mysql');
define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASS','');
define('DB_NAME','my_db');

define('HASH_ALGO','sha512');
define('HASH_KEY','mykey');
define('HASH_SECRET','my_secret');
define('SECRET_WORD','so_secret');
?>